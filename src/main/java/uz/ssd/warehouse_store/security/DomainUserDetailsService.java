//package uz.ssd.warehouse_store.security;
//
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.security.core.userdetails.UserDetails;
//import jakarta.transaction.Transactional;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.stereotype.Component;
//import uz.ssd.warehouse_store.entity.User;
//import uz.ssd.warehouse_store.service.UserService;
//
//import java.util.List;
//
///**
// * Authenticate a user from the database.
// */
//@Component(value = "userDetailsService")
//public class DomainUserDetailsService implements UserDetailsService
//{
//    //    private final Logger log = LoggerFactory.getLogger(DomainUserDetailsService.class);
//    private final UserService userService;
//    private final UserDetailService userDetailService;
//
//    public DomainUserDetailsService(UserService userService, UserDetailService userDetailService) {
//        this.userService = userService;
//        this.userDetailService = userDetailService;
//    }
//
//    @Override
//    @Transactional
//    public UserDetails loadUserByUsername(final String login) {
//        return loadByLogin(login);
//    }
//
//    public User loadByLogin(final String login) {
//        User user = userService.findByUsername(login);
//
//        if (user == null)
//            throw new UsernameNotFoundException("Пользователь " + login + "  не найден в базе");
//
//        return createUserPrincipal(user);
//    }
//
//    private User createUserPrincipal(User user) {
//        List<SimpleGrantedAuthority> authorities = getAuthorities(user);
//
//        User user1 = new User(
//                user.getId(),
//                user.getFirstName(),
//                user.getUsername(),
//                user.getPassword(),
//                false,
//                getAuthorities(user));
//
//        userPrincipal.setLogin(user.getLogin());
//        userPrincipal.setPassword(user.getPassword());
//        userPrincipal.setActive(user.getIsActive());
//
//        //TODO util class ochish kerak
//        if (authorities.contains(new SimpleGrantedAuthority("ROLE_CANDIDATE"))) {
//            UserDetail userDetail = userDetailService.findByUserId(user.getId());
//            userPrincipal.setUserDetail(userDetail);
//        }
//        return userPrincipal;
//    }
//
//    private List<SimpleGrantedAuthority> getAuthorities(User user) {
//        return user.getRoles().stream()
//                .map(authority -> new SimpleGrantedAuthority(authority.getName()))
//                .collect(Collectors.toList());
//
//    }
//
//}
