package uz.ssd.warehouse_store.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import uz.ssd.warehouse_store.entity.template.GenericEntity;

@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
@Entity
public class Warehouse extends GenericEntity {

    private String name;

    private String address;

    @ManyToOne
    private Organization organization;

}
