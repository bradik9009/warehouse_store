package uz.ssd.warehouse_store.entity;

import jakarta.persistence.Entity;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import uz.ssd.warehouse_store.entity.template.GenericEntity;

@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
@Entity
public class ProductType extends GenericEntity {

    private String name;

}
