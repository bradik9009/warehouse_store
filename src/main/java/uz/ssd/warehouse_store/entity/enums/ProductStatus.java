package uz.ssd.warehouse_store.entity.enums;

public enum ProductStatus {
    DEFECT,
    ACTIVE,
    SOLD_OUT
}
