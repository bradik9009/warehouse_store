package uz.ssd.warehouse_store.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.ManyToOne;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import uz.ssd.warehouse_store.entity.template.GenericEntity;

import java.time.LocalDate;

@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
@Entity
public class Income extends GenericEntity {

    @ManyToOne
    private ProductType productType;

    private LocalDate date;

    private Long count;
}
