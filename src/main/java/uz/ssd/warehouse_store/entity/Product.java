package uz.ssd.warehouse_store.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.ManyToOne;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import uz.ssd.warehouse_store.entity.enums.ProductStatus;
import uz.ssd.warehouse_store.entity.template.GenericEntity;

@EqualsAndHashCode(callSuper = true)
@Getter
@Setter
@Entity
public class Product extends GenericEntity {

    private String name;

    private Long incomePrice;

    private Long salePrice;

    private String code;

    @Enumerated(EnumType.STRING)
    private ProductStatus status;

    @ManyToOne
    private ProductType type;
}
