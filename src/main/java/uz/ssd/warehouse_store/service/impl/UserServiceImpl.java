package uz.ssd.warehouse_store.service.impl;

import uz.ssd.warehouse_store.entity.User;
import uz.ssd.warehouse_store.repository.UserRepository;
import uz.ssd.warehouse_store.service.UserService;

import java.util.stream.Collectors;

public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username).orElse(null);
    }
}
