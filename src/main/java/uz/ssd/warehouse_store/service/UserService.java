package uz.ssd.warehouse_store.service;

import uz.ssd.warehouse_store.entity.User;

public interface UserService {

    User findByUsername(String login);

}
