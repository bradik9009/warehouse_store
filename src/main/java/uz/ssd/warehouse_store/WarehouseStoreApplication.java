package uz.ssd.warehouse_store;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WarehouseStoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(WarehouseStoreApplication.class, args);
	}

}
